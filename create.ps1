﻿
$resWidth = 1024
$resHgt = 768
$ADdomain = "" # ie US\  WORK\
$domain = "" #change this to your fqdn ie example.net
$h = (Get-Content c:\users\administrator\Desktop\rdplist\servers.csv)  # change this to point to the csv file with all your hostsnames on seperate lines
$savePath = "c:\users\administrator\Desktop\rdplist\" # change this to where you want to save the RDP files
$hereString = @"

audiomode:i:2
authentication level:i:0
autoreconnection enabled:i:1
bitmapcachepersistenable:i:1
compression:i:1
disable cursor setting:i:0
disable full window drag:i:1
disable menu anims:i:1
disable themes:i:1
disable wallpaper:i:1
displayconnectionbar:i:1
keyboardhook:i:2
redirectclipboard:i:1
redirectcomports:i:0
redirectdrives:i:0
redirectprinters:i:0
redirectsmartcards:i:0
session bpp:i:16
prompt for credentials:i:0
promptcredentialonce:i:1
"@

 forEach ($hn in $h) {
  $out = @()
  $out += "full address:s:" + $hn + "." + $domain
  $out += "screen mode id:i:1"
  $out += "desktopwidth:i:" + $resWidth
  $out += "desktopheight:i:" + $resHgt
  $out += "username:s:" + $ADdomain + "\" + $hn.username
  $out += $hereString
  $outFileName = ".\" + $hn + ".rdp"
  $out | out-file $outFileName
  copy-item $outFileName $savePath  
 }  #closes foreach item
